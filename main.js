var express = require("express");
var app = express();

app.get("/", function(reg, res) {
       //Status - accepted
       res.status(202);
       //Mime type
       res.type("text/plain");
       res.send("The current time is " + (new Date()));
    }
);

app.get("/hello", function(reg, res) {
        //Status - accepted
        res.status(200);
        //Mime type
        res.type("text/html");
        res.send("<h1>HELLO</h1>");
    }
);

//3000 - port number
app.listen(3000, function() {
        console.info("Application started on port 3000");
    }
    );
